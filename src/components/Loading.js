import React from 'react'
import '../css/loading.css'

export default function Loading() {
    return (
        <div className='divPadre'>
            <div className='divHijo'>
                <div className="loader">Loading...</div>
            </div>
        </div>
    )
}