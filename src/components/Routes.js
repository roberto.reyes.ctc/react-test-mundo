import React from 'react'
import { BrowserRouter, Routes as Switch, Route } from "react-router-dom";
import Manteiner from '../pages/Manteiner'
import List from '../pages/List'

function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact element={<List />} />
                <Route path="/Mantenedor" element={<Manteiner />} />
                <Route path="/Mantenedor/:id" element={<Manteiner />} />
            </Switch>
        </BrowserRouter>
    )
}

export default Routes