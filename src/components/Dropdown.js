import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap'
import '../css/dropdown.css'

class DropdownComponent extends React.Component{
    state = {
        dropdown: false
    }
     
    render() {
        return(
            <div>
                {this.props.title}
                <Dropdown className="dropdown" isOpen={this.state.dropdown} toggle={() => this.setState({ dropdown: !this.state.dropdown})} disabled={this.props.disabled? true : false}>
                    <DropdownToggle color={'black'} caret className="dropdownButton" disabled={this.props.disabled? true : false}>
                        {this.props.selected || 'Seleccione...'}
                    </DropdownToggle>
                    <DropdownMenu>
                        {this.props.data.map(item => {
                            return <DropdownItem value={item.id} key={item.id} onClick={this.props.onChange}>{item.data}</DropdownItem>
                        })}
                    </DropdownMenu>
                </Dropdown>                
                <p className='required' hidden={!this.props.required}>Campo obligatorio</p>
            </div>
        )
    }
}

export default DropdownComponent