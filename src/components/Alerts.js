import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

export const ErrorMsg = (func) => {
    const Alert = withReactContent(Swal);

    Alert.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Se detectó un problema al intentar conectar con el servidor, por favor intente nuevamente mas tarde.',
        confirmButtonText: 'Vale',
        allowOutsideClick: false,
        allowEscapeKey: false
    }).then(() => {
        if(func !== undefined)
            func();
    })
}

export const SuccessMsg = (text, func) => {
    const Alert = withReactContent(Swal);

    Alert.fire({
        icon: 'success',
        title: '¡Éxito!',
        text,
        confirmButtonText: 'Vale',
        allowOutsideClick: false,
        allowEscapeKey: false
    }).then(() => {
        func();
    })
}
