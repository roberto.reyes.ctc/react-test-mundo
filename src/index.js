import React from 'react'
import ReactDOM from 'react-dom'
import Routes from './components/Routes'
import 'bootstrap/dist/css/bootstrap.css'

ReactDOM.render(<Routes />, document.getElementById('root'));
