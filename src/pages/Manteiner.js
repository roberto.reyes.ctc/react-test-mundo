import React from 'react'
import DropdownComponent from '../components/Dropdown'
import { ErrorMsg, SuccessMsg } from '../components/Alerts'
import '../css/manteiner.css'
import Loading from '../components/Loading'
import { Link, useNavigate } from 'react-router-dom'
import { useParams } from 'react-router'

class Component extends React.Component {
    state = {
        new: {},
        data: [],
        regiones: [],
        provincias: [],
        ciudades: [],
        calle: '',
        required: [false, false, false, false],
        loading: true
    }

    componentDidMount = async () => {
        await this.fetchData()
        if (this.props.id !== undefined)
            await this.fetchSetStreet()
        this.setState({ loading: false })
    }

    fetchSetStreet = async () => {
        await fetch('http://127.0.0.1:8000/api/MostrarCalle', {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                calle: this.props.id,
            })
        })
            .then((response) => response.json())
            .then(async (responseJsonFromServer) => {
                if (responseJsonFromServer.estado === "Done") {
                    let auxProvincias = this.state.data.find(item => item.id_region === responseJsonFromServer.data.id_region).provincias
                    let auxCiudades = auxProvincias.find(item => item.id_provincia === responseJsonFromServer.data.id_provincia).ciudades
                    let provincias = []
                    let ciudades = []

                    auxProvincias.map(provincia => {
                        return provincias.push({
                            id: provincia.id_provincia,
                            data: provincia.nombre_provincia
                        })
                    })

                    auxCiudades.map(ciudad => {
                        return ciudades.push({
                            id: ciudad.id_ciudad,
                            data: ciudad.nombre_ciudad
                        })
                    })

                    return this.setState({
                        region: responseJsonFromServer.data.nombre_region,
                        provincia: responseJsonFromServer.data.nombre_provincia,
                        ciudad: responseJsonFromServer.data.nombre_ciudad,
                        calle: responseJsonFromServer.data.nombre_calle,
                        new: {
                            id_region: responseJsonFromServer.data.id_region,
                            id_provincia: responseJsonFromServer.data.id_provincia,
                            id_ciudad: responseJsonFromServer.data.id_ciudad
                        },
                        provincias,
                        ciudades
                    })
                } else
                    return ErrorMsg(() => this.props.navigate("/", { replace: true }))
            })
            .catch(() => {
                return ErrorMsg(() => this.props.navigate("/", { replace: true }))
            })
    }

    fetchData = async () => {
        await fetch('http://127.0.0.1:8000/api/ListarRegiones')
            .then((response) => response.json())
            .then(async (responseJsonFromServer) => {
                let regiones = []
                responseJsonFromServer.map(region => {
                    return regiones.push({
                        id: region.id_region,
                        data: region.nombre_region
                    })
                })
                this.setState({ data: responseJsonFromServer, regiones })
            })
            .catch(() => {
                return ErrorMsg(() => this.props.navigate("/", { replace: true }))
            })
    }

    fetchNewStreet = async () => {
        this.setState({ loading: true })
        await fetch('http://127.0.0.1:8000/api/AgregarNuevaCalle', {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                calle: this.state.calle,
                ciudad: parseInt(this.state.new.id_ciudad)
            })
        })
            .then((response) => response.json())
            .then(async (responseJsonFromServer) => {
                if (responseJsonFromServer === "Done") {
                    this.setState({ loading: false })
                    return SuccessMsg("Los datos fueron ingresados exitosamente.", () => this.props.navigate("/", { replace: true }))
                } else {
                    this.setState({ loading: false })
                    return ErrorMsg(() => this.props.navigate("/", { replace: true }))
                }
            })
            .catch(() => {
                this.setState({ loading: false })
                return ErrorMsg(() => this.props.navigate("/", { replace: true }))
            })
    }

    fetchEditStreet = async () => {
        this.setState({ loading: true })
        await fetch('http://127.0.0.1:8000/api/ModificarCalle', {
            method: 'put',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                calle: this.state.calle,
                id_ciudad: parseInt(this.state.new.id_ciudad),
                id_calle: this.props.id
            })
        })
            .then((response) => response.json())
            .then(async (responseJsonFromServer) => {
                if (responseJsonFromServer === "Done") {
                    this.setState({ loading: false })
                    return SuccessMsg("Los datos fueron modificados exitosamente.", () => this.props.navigate("/", { replace: true }))
                } else {
                    this.setState({ loading: false })
                    return ErrorMsg(() => this.props.navigate("/", { replace: true }))
                }
            })
            .catch(() => {
                this.setState({ loading: false })
                return ErrorMsg(() => this.props.navigate("/", { replace: true }))
            })
    }

    handleRegionesChange = e => {
        let provincias = []
        let region = this.state.data.find(item => item.id_region === parseInt(e.target.value))

        region.provincias.map(provincia => {
            return provincias.push({
                id: provincia.id_provincia,
                data: provincia.nombre_provincia
            })
        })

        this.setState({
            new: {
                id_region: parseInt(e.target.value)
            },
            provincias,
            region: this.state.regiones.find(item => item.id === parseInt(e.target.value)).data,
            provincia: null,
            ciudad: null
        })
    }

    handleProvinceChange = e => {
        let ciudades = []
        let provincias = this.state.data.find(item => item.id_region === this.state.new.id_region).provincias
        let provincia = provincias.find(item => item.id_provincia === parseInt(e.target.value))

        provincia.ciudades.map(ciudad => {
            return ciudades.push({
                id: ciudad.id_ciudad,
                data: ciudad.nombre_ciudad
            })
        })

        this.setState({
            new: {
                ...this.state.new,
                id_provincia: parseInt(e.target.value),
                id_ciudad: null
            },
            ciudades,
            provincia: this.state.provincias.find(item => item.id === parseInt(e.target.value)).data,
            ciudad: null
        })
    }

    handleCitiesChange = e => {
        this.setState({
            new: {
                ...this.state.new,
                id_ciudad: parseInt(e.target.value)
            },
            ciudad: this.state.ciudades.find(item => item.id === parseInt(e.target.value)).data
        })
    }

    handleSubmit = async e => {
        e.preventDefault()

        let required = [false, false, false, false]

        if (this.state.calle.trim().length === 0)
            required[0] = true

        if (!this.state.new.id_region)
            required[1] = true

        if (!this.state.new.id_provincia)
            required[2] = true

        if (!this.state.new.id_ciudad)
            required[3] = true

        this.setState({ required })

        if (required.find(item => item === true))
            return

        if (this.props.id === undefined)
            this.fetchNewStreet()
        else
            this.fetchEditStreet()
    }

    render() {
        return (
            <div>
                <div className='background'>
                    <div className="manteinerContainer">
                        <h2>Mantenedor De Calle</h2>
                        <form className="manteinerForm" onSubmit={this.handleSubmit}>
                            <div className="form-group inputDiv">
                                <label>Nombre de la calle:</label>
                                <input
                                    type="text"
                                    className="form-control input"
                                    id="calle"
                                    value={this.state.calle}
                                    onChange={x => this.setState({ calle: x.target.value })}
                                    maxLength="250"
                                />
                                <label hidden={!this.state.required[0]} className='required'>Campo obligatorio</label>
                            </div>
                            <div className="dropdownContainer">
                                <DropdownComponent data={this.state.regiones} required={this.state.required[1]} onChange={this.handleRegionesChange} title={'Región:'} selected={this.state.region} />
                                <DropdownComponent data={this.state.provincias} required={this.state.required[2]} onChange={this.handleProvinceChange} title={'Provincia:'} selected={this.state.provincia} disabled={this.state.region ? false : true} />
                                <DropdownComponent data={this.state.ciudades} required={this.state.required[3]} onChange={this.handleCitiesChange} title={'Ciudad:'} selected={this.state.ciudad} disabled={this.state.provincia ? false : true} />
                            </div>
                            <button type='submit' className="btn submitButton">Guardar</button>
                            <Link to="/">{"< Volver"}</Link>
                        </form>
                    </div>
                    {this.state.loading &&
                        <Loading />
                    }
                </div>
            </div>
        )
    }
}

function Manteiner() {
    let params = useParams();
    let navigate = useNavigate();
    return <Component {...params} navigate={navigate} />
}

export default Manteiner