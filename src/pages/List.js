import React from "react"
import '../css/list.css'
import Loading from '../components/Loading'
import 'font-awesome/css/font-awesome.min.css'
import { Link } from 'react-router-dom'
import { ErrorMsg } from '../components/Alerts'

class List extends React.Component {
    state = {
        loading: true,
        calles: []
    }

    componentDidMount = async () => {
        this.fetchList()
    }

    fetchList = async () => {
        await fetch('http://127.0.0.1:8000/api/ListarCalles')
        .then((response) => response.json())
        .then(async (responseJsonFromServer) => {
            this.setState({ calles: responseJsonFromServer, loading: false })
        })
        .catch(() => {
            this.setState({ loading: false })
            return ErrorMsg()
        })
    }
 
    render() {
        return (
            <div>
                <div className='background'>
                    <div className="manteinerContainer col-7">
                        <h2>Lista de Calle</h2>
                        <div className="buttonDiv">
                            <Link to="/Mantenedor" type="button" className="btn btn-primary"><i className="fa fa-plus-circle"></i> Nueva Calle</Link>
                        </div>
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th width="45%" >Nombre</th>
                                    <th>Ciudad</th>
                                    <th>Provincia</th>
                                    <th>Región</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.calles.map(calle => {
                                    return <tr key={calle.id_calle}>
                                        <td>{calle.nombre_calle}</td>
                                        <td>{calle.nombre_ciudad}</td>
                                        <td>{calle.nombre_provincia}</td>
                                        <td>{calle.nombre_region}</td>
                                        <td><Link to={{pathname: "/Mantenedor/" + calle.id_calle}} type="button" className="btn btn-primary"><i className="fa fa-pencil"></i> Editar</Link></td>
                                    </tr>
                                })}
                            </tbody>
                        </table>
                        {this.state.calles.length === 0 && 
                            (
                                <p>No se encontraron calles en la base de datos.</p>
                            )
                        }
                    </div>
                    {this.state.loading &&
                        <Loading />
                    }
                </div>
            </div>
        )
    }
}

export default List