# React Test Mundo

Este proyecto está pensado para ser utilizado junto a [laravel-test-mundo](https://gitlab.com/roberto.reyes.ctc/laravel-test-mundo).

## Preparando el ambiente

Antes de pasar a la implementación, es necesario tener los siguientes sistemas previamente instalados.

- [laravel-test-mundo](https://gitlab.com/roberto.reyes.ctc/laravel-test-mundo)
- node versión 16.13.0
- npm versión 8.1.0

## Pasos para implementación

1. Ejecuta el proyecto de Laravel, `laravel_test_mundo`.
6. En la raíz del proyecto, ejecutar el comando `npm i`.
8. Arrancar el proyecto con el comando `npm start`.


